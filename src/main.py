import os

from flask import Flask
from flask import request
from flask import json

from app import service

def create_app(test_config=None):  
  # create and configure the app
  api = Flask(__name__)

  service.init()

  # Routes

  # Add a sensor reading
  @api.route('/sensor', methods=['POST'])
  def create():
    result = service.createReading(request.get_json(force=True))
    return json.jsonify(result)

  # Get a sensor reading by Sensor Id
  @api.route('/sensor/sensorId/<sensorId>', methods=['GET'])
  def getBySensorId(sensorId):
    result = service.getBySensorId(sensorId)
    return json.jsonify(result)

  # Get a list of available Sensor Id
  @api.route('/sensor/sensorId', methods=['GET'])
  def getSensorIds():
    result = service.getSensorIds()
    return json.jsonify(result)

  # Get a sensor reading record by reading type
  @api.route('/sensor/type/<type>', methods=['GET'])
  def getByReadingType(type):
    result = service.getByReadingType(type)
    return json.jsonify(result)

  # Get a list of available Reading types.
  @api.route('/sensor/type', methods=['GET'])
  def getReadingTypes():
    result = service.getReadingTypes()
    return json.jsonify(result)  
  
  return api

api = create_app()

if __name__ == '__main__':
    # Bind to PORT if defined, otherwise default to 5000.
    port = int(os.environ.get('PORT', 5000))
    api.run(host='0.0.0.0', port=port)