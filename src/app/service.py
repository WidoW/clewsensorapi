# service.py
#   Module implementing handlers for service requests
from app import model

# init()
#   Service initialization code
def init():
  print('INITIALIZING...')
  model.init()
  return

# createReading(data)
# params: 
#   data: data object containing sensor data to store in database
# return: 
#   returns the database entry id on success
#   or error with failed validation information on error
def createReading(data):
  print('CREATE READING, data: ', data)
  result = {}
  # Validate input has expected keys
  if not 'sensorId' in data:
    result['error'] = 'missing sensor id'    
  elif not 'timestamp' in data:
    result['error'] = 'missing time stamp'    
  elif not 'type' in data:
    result['error'] = 'missing type'
  elif not 'value' in data:
    result['error'] = 'missing value'    
  else:
    result['entryId'] = model.createReading(data)
  
  return result

def getBySensorId(sensorId):
  print('GET BY SENSOR ID, sensorId: ', sensorId)
  result = {}
  result['data'] = model.getBySensorId(sensorId)
  return result

def getSensorIds():
  print('GET SENSOR IDS')
  result = {}
  result['sensorIds'] = model.getSensorIds()
  return result

def getByReadingType(type):  
  print('GET BY READING TYPE, type=' + type)
  result = {}
  result['data'] = model.getByReadingType(type)
  return result

def getReadingTypes():
  print('GET READING TYPES')
  result = {}
  result['types'] = model.getReadingTypes()
  return result