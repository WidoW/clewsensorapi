# model.py
#   Module implementing the actual operations against the databse, used with a specific database
from tinydb import TinyDB, Query

# Todo: Database initialization data should be passed from configuration
# Relative db file location for running locally with flask
# db = TinyDB('./db/sensorReadings.json') 

# Absolute db file location for running inside Docker container 
db = TinyDB('/usr/src/db/sensorReadings.json')

# Initialize Query object for use in search
SensorReadings = Query()

# Placeholder for initializing database connection
def init():    
  print('INITIALIZING DATABASE')  
  return

# createReading() 
# params: 
#   data: data object containing sensor data to store in database
# return: 
#   returns the database entry id
def createReading(data):  
  return db.table('sensor-readings').insert(data)

# getBySensorId() 
# params: 
#   sensorId: id of the sensor for for data retrival 
# return: 
#   returns a list of all readings with given sensor id
def getBySensorId(sensorId):
  return db.table('sensor-readings').search(SensorReadings.sensorId == sensorId)

# getByReadingType() 
# params: 
#   type: type of sensor reading for for data retrival 
# return: 
#   returns a list of all readings with given reading type
def getByReadingType(type):
  return db.table('sensor-readings').search(SensorReadings.type == type)

# getReadingTypes() 
# return: 
#   returns a list of all reading types stored in databse
def getReadingTypes():
  readingsTable = db.table('sensor-readings')
  distinctTypes = list(set(map(lambda r: r['type'],readingsTable.all())))
  return distinctTypes

# getSensorIds() 
# return: 
#   returns a list of all sensor id's stored in databse
def getSensorIds():
  readingsTable = db.table('sensor-readings')
  distinctIds = list(set(map(lambda r: r['sensorId'],readingsTable.all()))) 
  return distinctIds