FROM python:3-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY ./src/requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY ./src /usr/src/app
COPY ./db /usr/src/db

# Expose the Flask port
EXPOSE 5000

CMD [ "python", "./main.py" ]
