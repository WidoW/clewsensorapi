.PHONY: build run stop inspect

IMAGE_NAME = widow/alpine-python3-flask
CONTAINER_NAME = clew-sensor-api
VOLUME_NAME = sensor-storage-volume

build:
	docker build -t $(IMAGE_NAME) .
	docker volume create --name $(VOLUME_NAME)

run:
	docker run -v $(VOLUME_NAME):/usr/src --rm -p 5000:5000 --name $(CONTAINER_NAME) $(IMAGE_NAME)	

inspect:
	docker inspect $(CONTAINER_NAME)

shell:
	docker exec -it $(CONTAINER_NAME) /bin/sh

stop:
	docker stop $(CONTAINER_NAME)
	